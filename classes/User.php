<?php

class User
{
    public function __construct(protected array $props)
    {
    }

    public function getProp($key): mixed
    {
        return $this->props[$key];
    }

    /**
     * @param array<Action> $allActions
     * @return void
     */
    public function showHisActions(array $allActions): void
    {
        echo $this->props['name'] . ":\n";
        foreach ($allActions as $action) {
            if ($action->checkUser($this))
                echo $action->getName() . ' ';
        }
        echo "\n--------\n";
    }
}
