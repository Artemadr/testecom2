<?php

class MoreFilter implements FilterInterface
{
    public function __construct(protected string $field, protected mixed $value)
    {
    }

    /**
     * @throws Exception
     */
    public function checkUser(User $user): bool
    {
        return $this->value < $user->getProp($this->field);
    }
}