<?php

interface FilterInterface
{
    public function checkUser(User $user): bool;
}