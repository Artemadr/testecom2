<?php

class Action
{
    public function __construct(protected AuditoryInterface $auditory, protected string $name)
    {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function checkUser(User $user): bool
    {
        return $this->auditory->checkUser($user);
    }
}