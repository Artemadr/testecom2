<?php

interface AuditoryInterface
{
    public function checkUser(User $user): bool;
}