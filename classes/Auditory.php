<?php

class Auditory implements AuditoryInterface
{
    /**
     * @param array<FilterInterface> $filters
     */
    public function __construct(protected array $filters)
    {
    }

    public function checkUser(User $user): bool
    {
        foreach ($this->filters as $filter) {
            if (!$filter->checkUser($user))
                return false;
        }
        return true;
    }
}