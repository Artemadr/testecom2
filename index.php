<?php
require "vendor/autoload.php";

$user1 = new User([
    'name' => 'Вася',
    'date' => strtotime('2022-10-01 13:00:00'),
    'revenue' => 1000
]);

$user2 = new User([
    'name' => 'Петя',
    'date' => strtotime('2022-10-01 14:00:00'),
    'revenue' => 2000
]);

$auditory1 = new Auditory([
    new LessFilter('date', strtotime('2022-10-01 13:00:01')),
    new MoreFilter('revenue',500)
]);

$auditory2 = new Auditory([
    new EqualFilter('name','Петя')
]);

$allActions = [
    new Action($auditory1, 'Акция 1'),
    new Action($auditory2, 'Акция 2'),
    new Action($auditory1, 'Акция 3'),
];

$user1->showHisActions($allActions);
$user2->showHisActions($allActions);
